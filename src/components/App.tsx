import * as React from "react";

class App extends React.Component {
	public render() {
		return (
			<div className="App">
				Hello React Typescript App
			</div>
		);
	}
}

export default App;
