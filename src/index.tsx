import * as React from "react";
import * as ReactDOM from "react-dom";

import "./index.styl";

import App from "./components/App";

ReactDOM.render(
	<App />,
	document.getElementById("root") as HTMLElement
);
